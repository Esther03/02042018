/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg02042018;
import java.util.Scanner;

/**
 *
 * @author arman
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    //Segunda versión: If anidado
    static void Funcion1(float x){
        if(x<-15){
            System.out.println(x+ " Está en el intervalo 1");
        }
        else{
            if(x<=1){
                System.out.println(x+ " Está en el intervalo 2");
            }
            else{
                if(x<10){
                    System.out.println(x+ "Está en el intervalo 3");
                }
                else{
                    if(x<25){
                        System.out.println(x+ " Está en el intervalo 4");
                    }
                    else{
                        System.out.println(x+ " Está en el intervalo 5");
                    }
                }
            }
        }
    }
    
    //Primera versión
    /* static void Funcion1 (float x){
        if (x<-15){
            System.out.println(x+ " Está en el intervalo 1");
        }
        if(x>=-15){
            if(x<=-1){
                System.out.println(x+ " Está en el intervalo 2");
            }
        }
        if(x>-1){
            if(x<10){
                System.out.println(x+ " Está en el intervalo 3");
            }
        }
        if(x>=10){
            if(x<25){
                System.out.println(x+ " Está en el intervalo 4");
            }
        }
        if (x>=25){
                System.out.println(x+ " Está en el intervalo 5");
        }
        
    }*/
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner valores=new Scanner (System.in);
        System.out.println("¿Cuál es el valor de x?");
        Funcion1(valores.nextFloat());
    }
    
}
